/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roll42.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 15:35:12 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/04 17:28:43 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROLL42_H
# define ROLL42_H

# include "libft_part_io.h"
# include "libft_part_list.h"
# include "libft_part_math.h"
# include "libft_part_memory.h"
# include "libft_part_string.h"
# include "libft_part_type.h"
# include "ft_printf.h"
# include "roll42_defines.h"
# include "roll42_typedefs.h"

t_bool				roll_get_opts(t_meta *meta, char **av);
t_bool				roll_print_help(void);

#endif
