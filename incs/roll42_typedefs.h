/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roll42_typedefs.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 15:37:51 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/04 18:12:20 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROLL42_TYPEDEFS_H
# define ROLL42_TYPEDEFS_H

typedef struct	s_meta {
	int				fd;
	short			opts;
	short			nb_opts;
}				t_meta;

typedef enum	e_opts {
	ROLL_OPTS_LOG,
	ROLL_OPTS_HELP
}				t_opts;

#endif
