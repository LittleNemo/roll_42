/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roll42_defines.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 15:36:58 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/04 18:26:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROLL42_DEFINES_H
# define ROLL42_DEFINES_H

# define ROLL_MSG_USAGE		"usage: ./roll42 [-h | --help] [-l | --log] command"
# define ROLL_MSG_OPEN		"Error(roll42): failing to open "
# define ROLL_MSG_READ		"Error(roll42): failing to read "
# define ROLL_MSG_CLOSE		"Error(roll42): failing to close "

# define ROLL_SRC_FILE		"/dev/urandom"
# define ROLL_LOG_FILE		"roll42.log"

#endif
