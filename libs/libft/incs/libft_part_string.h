/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_string.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:40:35 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/03 14:49:45 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_STRING_H
# define LIBFT_PART_STRING_H

# include <stdlib.h>
# include "libft_typedefs.h"
# include "libft_part_memory.h"
# include "libft_part_type.h"

/*
**	These functions are part of the string portion of the libft
*/
char			*ft_str_tolower(char *str);
char			*ft_str_toupper(char *str);
char			*ft_strcasechr(const char *s, int c);
char			*ft_strcasestr(const char *haystack, const char *needle);
char			*ft_strcat(char *s1, const char *s2);
char			*ft_strchr(const char *s, int c);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strctrim(const char *s, const char *charset);
char			*ft_strdup(const char *s1);
char			*ft_strjoin(const char *s1, const char *s2);
char			*ft_strmap(const char*s, char (*f)(char));
char			*ft_strmapi(const char*s, char (*f)(unsigned int, char));
char			*ft_strncasestr(const char *haystack, \
		const char *needle, size_t n);
char			*ft_strncat(char *s1, const char *s2, size_t n);
char			*ft_strncpy(char *dst, const char *src, size_t len);
char			*ft_strndup(const char *s1, size_t n);
char			*ft_strnew(size_t size);
char			*ft_strnstr(const char *haystack, const char *needle, size_t n);
char			*ft_strpbrk(const char *s1, const char *s2);
char			*ft_strrcasechr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strrev(char *str);
char			*ft_strstr(const char *haystack, const char *needle);
char			*ft_strsub(const char *s, unsigned int start, size_t len);
char			*ft_strtok(char *str, const char *charset);
char			*ft_strtrim(const char *s);
char			**ft_strsplit(const char *s, char c);
char			**ft_strusplit(const char *s, char *charset);
int				ft_strcasecmp(const char *s1, const char *s2);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strequ(const char*s1, const char*s2);
int				ft_strncasecmp(const char *s1, const char *s2, size_t n);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
int				ft_strnequ(const char *s1, const char *s2, size_t n);
int				ft_tolower(int c);
int				ft_toupper(int c);
size_t			ft_strcprg(char *str, const char *charset, size_t n);
size_t			ft_strprg(char *str, char c, size_t n);
size_t			ft_strcspn(const char *s, const char *charset);
size_t			ft_strlcat(char *dst, const char *src, size_t size);
size_t			ft_strlen(const char *s);
size_t			ft_strnlen(const char *s, size_t maxlen);
size_t			ft_strspn(const char *s, const char *charset);
size_t			ft_tablen(char **tab);
t_bool			ft_strclr(char *s);
t_bool			ft_strdel(char **as);
t_bool			ft_striter(char *s, void (*f)(char *));
t_bool			ft_striteri(char *s, void (*f)(unsigned int, char *));
t_bool			ft_tabdel(void **tab);

#endif
