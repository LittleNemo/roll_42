/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprime.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 12:09:41 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:40:37 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_math.h"

/*
** DESCRIPTION
**	- The ft_isprime() function tests if the integer n is a prime number.
**	We concider all the prime numbers except 0, 1 and, of course,
**	every negativ numbers.
**
**	RETURN VALUES
**	- ft_isprime() returns 1 if the test is true or 0 if the test is false.
*/

int				ft_isprime(int n)
{
	int				i;

	if (n <= 1)
		return (false);
	i = 2;
	while (i < n)
		if (!(n % i++))
			return (false);
	return (true);
}
