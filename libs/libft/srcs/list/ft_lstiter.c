/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/20 16:01:21 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:32:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_list.h"

/*
** DESCRIPTION
**	- The ft_lstiter() function applies the f function on every links of the
**	linked list pointed by lst.
**
** NOTE
**	- See libft_typedefs.h to see the t_list structure.
*/

void			ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	if (!lst || !f)
		return ;
	while (lst)
	{
		f(lst);
		lst = lst->next;
	}
}
