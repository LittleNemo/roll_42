/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasechr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 18:53:09 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:16:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strcasechr() function locates the first occurence of c (converted
**	to a char) in the string s, ignoring the case. The null-terminating
**	character is concidered to be part of s. This means if c is '\0', the
**	function will locate the terminating '\0'.
**
** RETURN VALUES
**	- ft_strcasechr() returns a pointer to the located character, or NULL if c
**	does not appear in s.
*/

char			*ft_strcasechr(const char *s, int c)
{
	while (ft_tolower(*s) != ft_tolower(c))
		if (!*s++)
			return (NULL);
	return ((char *)s);
}
