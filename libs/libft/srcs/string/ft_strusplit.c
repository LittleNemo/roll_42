/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strusplit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/25 11:52:14 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:29:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strusplit() function allocates an null-terminated array of
**	null-terminated strings representing the spliting of the string s on every
**	characters of the string charset whitin it.
**
** RETURN VALUES
**	- ft_strusplit() returns a pointer to the allocated array, or NULL if an
**	error occured.
*/

static size_t	ft_cntwrd(const char *s, char *charset)
{
	size_t			nb_wrd;

	nb_wrd = 0;
	while (*s)
	{
		s += ft_strcspn(s, charset);
		s += ft_strspn(s, charset);
		if (*s)
			nb_wrd++;
	}
	return (nb_wrd);
}

char			**ft_strusplit(const char *s, char *charset)
{
	char			**tab;
	size_t			len;
	size_t			i;

	if (!s || !charset || !(tab = (char **)ft_memalloc(sizeof(tab) \
		* (ft_cntwrd(s, charset) + 1))))
		return (NULL);
	i = 0;
	s += ft_strspn(s, charset);
	while (*s)
	{
		len = ft_strcspn(s, charset);
		if (!(tab[i++] = ft_strndup(s, len)))
			return (NULL);
		s += len + ft_strspn(s, charset);
	}
	return (tab);
}
