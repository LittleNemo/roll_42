/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strctrim.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 15:02:06 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:19:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strctrim() function copies the string s without any character of
**	the string charset at the start and end of s.
**
** RETURN VALUES
**	- ft_strctrim() returns a pointer to the fresh string. If an error occured,
**	a null-pointer is return instead.
*/

char			*ft_strctrim(const char *s, const char *charset)
{
	size_t			len;

	if (!s || !charset)
		return (NULL);
	s += ft_strspn(s, charset);
	len = ft_strlen(s) - 1;
	while (ft_strchr(charset, s[len]))
		len--;
	return (ft_strndup(s, len + 1));
}
