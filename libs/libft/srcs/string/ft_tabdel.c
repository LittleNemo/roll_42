/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 10:23:02 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:39:46 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_tabdel() frees the memory allocated for every address of the string
**	tab, then free the memory of the string.
**
** RETURN VALUE
**	- ft_rabdel() returns true if the array has succesfully be deleted.
**	Otherwise, false is returned.
*/

t_bool			ft_tabdel(void **tab)
{
	int				i;

	if (!tab)
		return (false);
	i = -1;
	while (tab[++i])
		free(tab[i]);
	free(tab);
	return (true);
}
