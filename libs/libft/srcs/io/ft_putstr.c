/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 13:35:42 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:18:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_putstr() function attempts to write the string s on the
**	standard output, a.k.a. stdout.
**
** RETURN VALUES
**	- ft_putstr() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putstr(char const *s)
{
	return (write(1, s, ft_strlen(s)));
}
