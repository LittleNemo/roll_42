/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roll_get_opts.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 16:34:59 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/04 17:29:18 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "roll42.h"

t_bool			roll_print_help(void)
{
	ft_fprintf(stdout, "%s\n", ROLL_MSG_USAGE);
	ft_fprintf(stdout, "Help:\n");
	return (true);
}

t_bool			roll_get_opts(t_meta *meta, char **av)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (av[++i] && av[i][0] == '-')
	{
		j = 0;
		while (av[i][++j])
			if (ft_strequ("-log", av[i] + 1) || av[i][j] == 'l')
				meta->opts |= (1 << ROLL_OPTS_LOG);
			else if (ft_strequ("-help", av[i] + 1) || av[i][j] == 'h')
				meta->opts |= (1 << ROLL_OPTS_HELP);
			else
				return (true);
		meta->nb_opts++;
	}
	return (false);
}
