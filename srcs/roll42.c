/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   roll42.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 15:33:08 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/05 13:18:36 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "roll42.h"

t_bool			roll_dice(int fd, int *total, unsigned int type)
{
	unsigned char	random;
	unsigned int	roll;

	if (read(fd, &random, 1) == -1)
		return (!!ft_fprintf(stderr, "%s%s\n", ROLL_MSG_READ, ROLL_SRC_FILE));
	roll = (random % type) + 1;
	ft_printf("%d", roll);
	*total += roll;
	return (false);
}

t_bool			roll_fate(int fd, int *total)
{
	unsigned char	random;
	t_bool			roll;

	if (read(fd, &random, 1) == -1)
		return (!!ft_fprintf(stderr, "%s%s\n", ROLL_MSG_READ, ROLL_SRC_FILE));
	roll = ((random % 6) / 2) - 1;
	if (roll == -1)
		ft_printf("-");
	else if (roll == 1)
		ft_printf("+");
	else
		ft_printf("0");
	*total += roll;
	return (false);
}

int				main(int ac, char **av)
{
	static t_meta	meta;

	if (roll_get_opts(&meta, av))
		return (!!ft_fprintf(stderr, "%s\n", ROLL_MSG_USAGE));
	if (meta.opts & (1 << ROLL_OPTS_HELP))
		return (roll_print_help());
	if (!(ac - meta.nb_opts - 1))
		return (!!ft_fprintf(stderr, "%s%s\n", ROLL_MSG_USAGE));
	if ((meta.fd = open(ROLL_SRC_FILE, O_RDONLY)) == -1)
		return (!!ft_fprintf(stderr, "%s%s\n", ROLL_MSG_OPEN, ROLL_SRC_FILE));
	ft_printf("rolling dice\n");
	if (close(meta.fd) == -1)
		return (!!ft_fprintf(stderr, "%s%s\n", ROLL_MSG_CLOSE, ROLL_SRC_FILE));
	return (0);
}
